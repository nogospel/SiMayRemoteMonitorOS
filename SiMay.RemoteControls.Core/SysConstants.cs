﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.RemoteControls.Core
{
    public class SysConstants
    {
        public const int INDEX_COUNT = 3;
        public const int INDEX_WORKTYPE = 0;
        public const int INDEX_WORKER = 1;
        public const int INDEX_SYNC_SEQUENCE = 2;

        public const string IPV4 = "IPV4";
        public const string MachineName = "MachineName";
        public const string Remark = "Remark";
        public const string ProcessorInfo = "ProcessorInfo";
        public const string ProcessorCount = "ProcessorCount";
        public const string MemorySize = "MemorySize";
        public const string StartRunTime = "StartRunTime";
        public const string ServiceVison = "ServiceVison";
        public const string UserName = "UserName";
        public const string OSVersion = "OSVersion";
        public const string GroupName = "GroupName";
        public const string ExistCameraDevice = "ExistCameraDevice";
        public const string ExitsRecordDevice = "ExitsRecordDevice";
        public const string ExitsPlayerDevice = "ExitsPlayerDevice";
        public const string IdentifyId = "IdentifyId";
        public const string Session = "Session";
    }
}
